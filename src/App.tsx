import React, {useEffect, useState} from 'react';
import UserSignUp from "./components/UserSignUp";
import {Button, Container, Grid, Typography} from "@material-ui/core";
import UserSignIn from "./components/UserSignIn";
import UserProfile from "./components/UserProfile";
import {TOKEN_KEY} from "./constants/constants";

function App() {
    const [view, setView] = useState<string>("");

    useEffect(() => {
        const token = localStorage.getItem(TOKEN_KEY);
        if (token) {
            setView("profile");
        } else {
            setView("signin")
        }
    }, []);

    return (
        <div className="App">
            <Container style={{maxWidth: "500px"}}>
                <Typography
                    gutterBottom
                    variant="h3"
                    style={{textAlign: "center"}}
                >
                    Account
                </Typography>

                {
                    view === "signin" &&
                        <>
                            <UserSignIn afterSignIn={() => setView("profile")}/>
                            <Grid container style={{marginTop: "10px"}} alignItems={"center"}>
                            <Typography>
                                    If you don't have an account yet, you can
                                </Typography>
                                <Button
                                    color={"primary"}
                                    onClick={() => setView("signup")}
                                >
                                    Sign Up
                                </Button>
                                <Typography>
                                    here
                                </Typography>
                            </Grid>
                        </>
                }
                {
                    view === "profile" &&
                    <UserProfile onLogOut={() => setView("signin")}/>
                }
                {
                    view === "signup" &&
                    <>

                        <UserSignUp afterSignUp={() => setView("signin")}/>
                        <Grid container style={{marginTop: "10px"}} alignItems={"center"}>
                            <Typography>
                                If you already have an account you can
                            </Typography>
                            <Button
                                color={"primary"}
                                onClick={() => setView("signin")}
                            >
                                Sign In
                            </Button>
                            <Typography>
                                here
                            </Typography>
                        </Grid>
                    </>
                }
            </Container>
        </div>
    );
}

export default App;
