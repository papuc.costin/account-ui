import {
    Button, Card, CardContent,
    Grid,
    TextField,
    Typography,
} from "@material-ui/core";
import React, {useState} from "react";
import {URLS} from "../constants/constants";
import axios from 'axios';
import {CreateUser} from "../model/CreateUser";

export interface UserSignUpProps {
    afterSignUp: () => void;
}

function UserSignUp({afterSignUp}: UserSignUpProps) {
    const [form, setForm] = useState<CreateUser>({} as CreateUser);
    const [error, setError] = useState<string>("");
    const [message, setMessage] = useState<string>("");
    const [formErrors, setFormErrors] = useState<any>({});
    const [formErrorsMessage, setFormErrorsMessage] = useState<any>({});

    const validateField = (key: keyof CreateUser) => {
        if (!form[key]) {
            setFormErrors((prevState: any) => ({...prevState, [key]: true}));
            setFormErrorsMessage((prevState: any) => ({...prevState, [key]: "Mandatory field"}));
            return false;
        }
        return true;
    };

    const validatePassword = () => {
        if (form.password && form.password.length < 6) {
            setFormErrors((prevState: any) => ({...prevState, password: true}));
            setFormErrorsMessage((prevState: any) =>
                ({
                    ...prevState,
                    password: "Password must have at least 6 characters"
                })
            );
            return false;
        }
        return true;
    };

    const validate = () => {
        return [
            validateField("uniqueId"),
            validateField("password"),
            validatePassword(),
            validateField("firstName"),
            validateField("lastName"),
            validateField("phoneNumber"),
            validateField("email")
        ].reduce((a, b) => a && b)
    };

    const signUp = () => {
        setError("");
        setFormErrors({});
        if (!validate()) {
            return;
        }

        axios.post(URLS.backend + '/users', form)
            .then(res => {
                setMessage("Successfully signed up. Redirect to sign in...");
                setTimeout(() => afterSignUp(), 3000);
            })
            .catch(error => {
                console.log(error.message);
                setError(error?.response?.data?.message ?? error.message);
            })
    };

    return (
        <Card elevation={2}>
            <CardContent>
                <Grid container direction="row" alignItems="flex-start" spacing={2}>
                    <Grid item xs={12}>
                        <Typography color="error">{error}</Typography>
                    </Grid><Grid item xs={12}>
                    <Typography color="primary">{message}</Typography>
                </Grid>
                    <Grid item xs={12}>
                        <TextField
                            error={formErrors.uniqueId}
                            label={formErrorsMessage.uniqueId ?? "Unique id"}
                            fullWidth
                            multiline
                            required
                            value={form.uniqueId ?? ""}
                            placeholder={"Unique id"}
                            onChange={(event) => {
                                const val = event.currentTarget.value;
                                setForm(prevState => ({...prevState, uniqueId: val}))
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            error={formErrors.password}
                            label={formErrorsMessage.password ?? "Password"}
                            fullWidth
                            multiline
                            required
                            value={form.password ?? ""}
                            placeholder={"Password"}
                            onChange={(event) => {
                                const val = event.currentTarget.value;
                                setForm(prevState => ({...prevState, password: val}))
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            error={formErrors.firstName}
                            label={formErrorsMessage.firstName ?? "First name"}
                            fullWidth
                            multiline
                            required
                            value={form.firstName ?? ""}
                            placeholder={"First name"}
                            onChange={(event) => {
                                const val = event.currentTarget.value;
                                setForm(prevState => ({...prevState, firstName: val}))
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            error={formErrors.lastName}
                            label={formErrorsMessage.lastName ?? "Last name"}
                            fullWidth
                            multiline
                            required
                            value={form.lastName ?? ""}
                            placeholder={"Last name"}
                            onChange={(event) => {
                                const val = event.currentTarget.value;
                                setForm(prevState => ({...prevState, lastName: val}))
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            error={formErrors.phoneNumber}
                            label={formErrorsMessage.phoneNumber ?? "Phone number"}
                            fullWidth
                            multiline
                            required
                            value={form.phoneNumber ?? ""}
                            placeholder={"Phone number"}
                            onChange={(event) => {
                                const val = event.currentTarget.value;
                                setForm(prevState => ({...prevState, phoneNumber: val}))
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            error={formErrors.email}
                            label={formErrorsMessage.email ?? "Email"}
                            fullWidth
                            multiline
                            required
                            value={form.email ?? ""}
                            placeholder={"Email"}
                            onChange={(event) => {
                                const val = event.currentTarget.value;
                                setForm(prevState => ({...prevState, email: val}))
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            variant={"contained"}
                            color={"primary"}
                            onClick={signUp}
                        >
                            Sign up
                        </Button>
                    </Grid>

                </Grid>
            </CardContent>
        </Card>
    );
}

export default UserSignUp;
