import {
    Button,
    Card, CardActions, CardContent,
    Grid,
    Typography,
} from "@material-ui/core";
import React, {useEffect, useState} from "react";
import {User} from "../model/User";
import {TOKEN_KEY, URLS} from "../constants/constants";
import axios from "axios";
import jwt from "jwt-decode"

export interface UserProfileProps {
    onLogOut: () => void;
}

function UserProfile({onLogOut}: UserProfileProps) {
    const [user, setUser] = useState<User | undefined>(undefined);

    useEffect(() => {
        const token = localStorage.getItem(TOKEN_KEY);
        if (!token) {
            onLogOut();
            return;
        }
        const decodedJwt: any = jwt(token);
        console.log(decodedJwt);

        const config = {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        };
        axios.get(URLS.backend + '/users/' + decodedJwt.userUuid, config)
            .then(res => {
                console.log(res.data);
                setUser(res.data);
            })
            .catch(error => {
                console.log(error);
            })
    }, [onLogOut]);

    const logOut = () => {
        localStorage.removeItem("auth.token");
        onLogOut();
    };

    return (
        <Card elevation={2}>
            <CardContent>
                {
                    user &&
                    <Grid>
                        <Typography>
                            First Name: {user.firstName}
                        </Typography>
                        <Typography>
                            Last Name: {user.lastName}
                        </Typography>
                        <Typography>
                            Phone Number: {user.phoneNumber}
                        </Typography>
                        <Typography>
                            Email: {user.email}
                        </Typography>
                    </Grid>
                }
            </CardContent>
            <CardActions>
                <Button
                    variant={"contained"}
                    onClick={logOut}
                >
                    Log out
                </Button>
            </CardActions>
        </Card>
    );
}

export default UserProfile;
