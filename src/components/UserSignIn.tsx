import {
    Button, Card, CardContent,
    Grid,
    TextField,
    Typography,
} from "@material-ui/core";
import React, {useState} from "react";
import {TOKEN_KEY, URLS} from "../constants/constants";
import axios from 'axios';
import {AuthUser} from "../model/AuthUser";


export interface UserSignInProps {
    afterSignIn: () => void
}

function UserSignIn({afterSignIn}: UserSignInProps) {
    const [form, setForm] = useState<AuthUser>({} as AuthUser);
    const [error, setError] = useState<string>("");

    const signIn = () => {
        axios.post(URLS.backend + '/login', form)
            .then(res => {
                localStorage.setItem(TOKEN_KEY, res.headers.authorization.replace("Bearer ", ""));
                afterSignIn();
            })
            .catch(error => {
                console.log(error.message);
                setError(error?.response?.data?.message ?? error.message);
            })
    };

    return (
        <Card elevation={2}>
            <CardContent>
                <Grid container direction="row" alignItems="flex-start" spacing={2}>
                    <Grid item xs={12}>
                        <Typography color="error">{error}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label={"Unique id"}
                            fullWidth
                            multiline
                            required
                            value={form.uniqueId ?? ""}
                            placeholder={"Unique id"}
                            onChange={(event) => {
                                const val = event.currentTarget.value;
                                setForm(prevState => ({...prevState, uniqueId: val}))
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label={"Password"}
                            fullWidth
                            multiline
                            required
                            value={form.password ?? ""}
                            placeholder={"Password"}
                            onChange={(event) => {
                                const val = event.currentTarget.value;
                                setForm(prevState => ({...prevState, password: val}))
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            variant={"contained"}
                            color={"primary"}
                            onClick={signIn}
                        >
                            Sign in
                        </Button>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    );
}

export default UserSignIn;
