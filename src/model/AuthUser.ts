export interface AuthUser {
    uniqueId: string;
    password: string;
}