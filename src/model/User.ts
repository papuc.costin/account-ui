export interface User {
    uuid: string;

    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;

    createdTimestamp: string;
    updatedTimestamp: string;
}