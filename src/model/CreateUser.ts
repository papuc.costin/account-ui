export interface CreateUser {
    uniqueId: string;
    password: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    email: string;
}